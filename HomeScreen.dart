import 'dart:io';

import 'package:flutter/material.dart';
//import 'package:laundryapp/Clients.dart';
import 'package:flutter/foundation.dart';
import 'package:laundryapp/SignIn.dart';

class HomeScreen extends StatefulWidget {
  Clients currentUser;
  HomeScreen({Key? key, required this.currentUser});

  // HomeScreen({
  //   @required currentUser,
  //   Key? key,
  // }) : super(key: key);
  @override
  _MainScreenState createState() => _MainScreenState(currentUser);
}

//class MainScreen extends StatelessWidget {
class _MainScreenState extends State<HomeScreen> {
  Clients userValue;
  _MainScreenState(this.userValue);

  //MainScreen({Key? key}) : super(key: key);
  //var navPages = [Profile(profileuser: userValue), HomePage()];
  //Clients currentClient = userValue;
  int clientIndex = 0;
  int _selectedIndex = 1;
  //_selectedIndex = userValue.currentIndex;

  //final userController = TextEditingController();
  //List<Widget> pages = [buildbody(), Text("Hello")];
  //var widgets = {buildbody(): '1'};

  @override
  Widget build(BuildContext context) {
    var navPages = [
      Profile(profileuser: userValue),
      HomePage(),
      MoreScreen(profileuser: userValue)
    ];
    clientIndex = userValue.currentIndex;
    //navPages[0] = Profile(profileuser: userValue);
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        //title: Text("${widget.currentUser.nameList[0]}"),
        title: Text("${userValue.nameList[clientIndex]}"),
        // title: Text(
        //     "${widget.currentUser.nameList[widget.currentUser.currentIndex]}"),
        // title: Text("${userValue.nameList[userValue.currentIndex]}"),
        centerTitle: true,
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: "Profile",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: "Home",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.menu),
            label: "More",
          ),
        ],
        currentIndex: _selectedIndex,
        onTap: (_onItemTapped),
      ),
      body: //_buildBody(),
          Column(
        children: [
          SizedBox(
            height: 115,
            width: 115,
            child: Stack(),
          ),
          Center(
            child: navPages.elementAt(_selectedIndex),
          ),
          Container(
            padding: const EdgeInsets.all(10),
            child: Text("${userValue.currentIndex}"),
          ),
          // Center(
          //   child: ElevatedButton(
          //     onPressed: () {},
          //     child: const Text("Book Machine"),
          //   ),
          // ),
          // Center(
          //     //child: _pages[_selectedIndex],
          //     ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.red,
          child:
              // const Icon(Icons.door_back_door_outlined),
              Container(
            //height: 115,
            padding: const EdgeInsets.all(10),
            child: const Text("LOGOUT"),
          ),
          onPressed: () {
            //Navigator.pop(context);
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return SignIn(
                currentList: userValue,
              );
            }));
            //_logOuUser();
            //userValue = Clients.createnew(true);
          }),
    );
  }

  void _logOuUser() {
    setState(() {
      widget.currentUser = Clients.createnew(true);
    });
  }

  void buildbody() {
    Widget _buildBody() {
      return Container(
          //color: activeTabColor[TabItem.red],
          alignment: Alignment.center,
          child: ElevatedButton(
            child: const Text(
              'PUSH',
              style: TextStyle(fontSize: 32.0, color: Colors.white),
            ),
            onPressed: () {},
          ));
    }
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
      userValue = widget.currentUser;

      //navPages[0] = Profile(profileuser: widget.currentUser);
    });
  }
}

class Profile extends StatefulWidget {
  Profile({Key? key, required this.profileuser});

  Clients profileuser;
  //Clients currentUser;

  //Profile({Key key, @required this.currentUser}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  var nameController = TextEditingController();
  var usernameController = TextEditingController();
  bool isEnabled = false;
  String btnEditText = "Edit";

  @override
  Widget build(BuildContext content) {
    nameController.text =
        "${widget.profileuser.nameList[widget.profileuser.currentIndex]}";
    usernameController.text =
        "${widget.profileuser.userList[widget.profileuser.currentIndex]}";
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.all(10),
          child: TextField(
            enabled: isEnabled,
            controller: nameController,
            obscureText: false,
            decoration: InputDecoration(
                border: OutlineInputBorder(), labelText: "Customer Name"),
          ),
        ),
        Container(
          padding: const EdgeInsets.all(10),
          child: TextField(
            enabled: isEnabled,
            controller: usernameController,
            obscureText: false,
            decoration: InputDecoration(
                border: OutlineInputBorder(), labelText: "Customer Username"),
          ),
        ),
        Container(
          width: 225,
          padding: EdgeInsets.all(10),
          child: ElevatedButton(
            onPressed: () {
              //btnEditText = "Cancel";
              //isEnabled = true;
              _EnableEdit();
            },
            child: Text(btnEditText),
          ),
        ),
        Container(
          width: 225,
          padding: EdgeInsets.all(10),
          child: ElevatedButton(
            onPressed: () {
              _SaveChanges();
              btnEditText = "Edit";
            },
            child: const Text("Save Changes"),
          ),
        ),
      ],
    );
  }

  void _EnableEdit() {
    setState(() {
      isEnabled = true;
      if (btnEditText == "Cancel") {
        isEnabled = false;
        btnEditText = "Edit";
      }

      if (isEnabled == true) {
        btnEditText = "Cancel";
      }
      //btnEditText = "Cancel";

      // if (isEnabled == true && btnEditText == "Cancel") {
      //   isEnabled = false;
      // } else {
      //   isEnabled = true;
      // }

      // if (btnEditText == "Cancel" && isEnabled == false) {
      //   btnEditText == "Edit";
      // } else {
      //   btnEditText = "Cancel";
      // }
    });
  }

  void _SaveChanges() {
    setState(() {
      widget.profileuser.nameList[widget.profileuser.currentIndex] =
          nameController.text;
      widget.profileuser.userList[widget.profileuser.currentIndex] =
          usernameController.text;
      isEnabled = false;
    });
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext content) {
    return Column(
      children: [
        Center(
          child: Container(
            padding: const EdgeInsets.all(10),
            width: 225,
            child: ElevatedButton(
              onPressed: () {},
              child: const Text("Book Machine"),
            ),
          ),
        ),
        Container(
          padding: const EdgeInsets.all(10),
          width: 225,
          child: ElevatedButton(
            onPressed: () {},
            child: const Text("View History"),
          ),
        ),
        Container(
          padding: const EdgeInsets.all(10),
          width: 225,
          child: ElevatedButton(
            onPressed: () {},
            child: const Text("About"),
          ),
        ),
        // Container(
        //   padding: const EdgeInsets.all(10),
        //   child: ElevatedButton(
        //     onPressed: () {},
        //     child: const Text("Log Out"),
        //   ),
        // ),
      ],
    );
  }
}

class MoreScreen extends StatefulWidget {
  MoreScreen({Key? key, required this.profileuser});

  Clients profileuser;
  //Clients currentUser;

  //Profile({Key key, @required this.currentUser}) : super(key: key);

  @override
  _MoreScreenState createState() => _MoreScreenState();
}

class _MoreScreenState extends State<MoreScreen> {
  var nameController = TextEditingController();
  var usernameController = TextEditingController();
  bool isEnabled = false;
  String btnEditText = "Edit";

  @override
  Widget build(BuildContext content) {
    return Column(
      children: [
        Center(
          child: Container(
            width: 225,
            padding: const EdgeInsets.all(10),
            child: ElevatedButton(
              onPressed: (() {}),
              child: const Text("Settings"),
            ),
          ),
        ),
        Center(
          child: Container(
            width: 225,
            padding: const EdgeInsets.all(10),
            child: ElevatedButton(
              onPressed: (() {}),
              child: const Text("Make Donation"),
            ),
          ),
        ),
        Center(
          child: Container(
            width: 225,
            padding: const EdgeInsets.all(10),
            child: ElevatedButton(
              onPressed: (() {}),
              child: const Text("Feedback"),
            ),
          ),
        ),
      ],
    );
  }
}
