import 'package:flutter/material.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:laundryapp/SignIn.dart';

class Splash extends StatelessWidget {
  const Splash({Key? key}) : super(key: key);

  //Clients baseclient;

  @override
  Widget build(BuildContext context) {
    Container(
      // textStyle: const TextStyle(
      //   color: Colors.White,
      // ),
      child: const Text("AWOKEN"),
    );
    return AnimatedSplashScreen(
      splash: Container(
        width: 250,
        height: 250,
        //color: Colors.cyan,
        child: Image.asset('assets/laundryAppV1.png'),
      ),
      splashIconSize: 500,
      backgroundColor: Colors.white,
      nextScreen: SignIn(currentList: Clients()),
      splashTransition: SplashTransition.sizeTransition,
    );
  }
}
