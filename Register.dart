import 'package:laundryapp/HomeScreen.dart';
import 'package:laundryapp/SignIn.dart';
import 'package:flutter/material.dart';

class Register extends StatefulWidget {
  Clients baseList;
  Register({Key? key, required this.baseList});

  @override
  _RegisterState createState() => _RegisterState(baseList);
}

class _RegisterState extends State<Register> {
  var newUser = Clients();
  _RegisterState(this.newUser);

  //Clients newUser = Clients.createnew(true);
  final newNameController = TextEditingController();
  final newUsernameController = TextEditingController();
  final newPasswordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: const Text("Register user"),
        centerTitle: true,
      ),
      body: Column(children: [
        SizedBox(
          height: 115,
          width: 115,
          child: Stack(),
        ),
        Container(
          padding: const EdgeInsets.all(10),
          child: Text("${newUser.nameList}"),
        ),
        Container(
          padding: const EdgeInsets.all(10),
          child: TextField(
            controller: newNameController,
            obscureText: false,
            decoration: InputDecoration(
                border: OutlineInputBorder(), labelText: 'Enter Name'),
          ),
        ),
        Container(
          padding: const EdgeInsets.all(10),
          child: TextField(
            controller: newUsernameController,
            obscureText: false,
            decoration: InputDecoration(
                border: OutlineInputBorder(), labelText: 'Enter Username'),
          ),
        ),
        Container(
          padding: const EdgeInsets.all(10),
          child: TextField(
            controller: newPasswordController,
            obscureText: false,
            decoration: InputDecoration(
                border: OutlineInputBorder(), labelText: 'Enter Password'),
          ),
        ),
        Container(
          padding: const EdgeInsets.all(10),
          child: ElevatedButton(
            onPressed: () {
              if (newNameController.text != "" ||
                  newPasswordController.text != "" ||
                  newUsernameController.text != "") {
                _createNewUser();
              }
              //print(newUser.nameList);
              // Navigator.push(context, MaterialPageRoute(builder: (context) {
              //   return HomeScreen(
              //     currentUser: newUser,
              //   );
              // }));
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) {
                return SignIn(
                  currentList: newUser,
                );
              }));
            },
            child: const Text("Complete Registration"),
          ),
        ),
        Container(
          padding: const EdgeInsets.all(10),
          child: ElevatedButton(
            onPressed: () {
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) {
                return SignIn(
                  currentList: widget.baseList,
                );
              }));
            },
            child: const Text("Back"),
          ),
        ),
      ]),
    );
  }

  void _createNewUser() {
    setState(() {
      newUser.nameList.add(newNameController.text);
      newUser.userList.add(newUsernameController.text);
      newUser.passwordList.add(newPasswordController.text);
      // newNameController.text;
    });
  }
}
