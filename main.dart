import 'package:flutter/material.dart';
import 'package:laundryapp/Splash.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: Splash(),
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.cyan)
            .copyWith(secondary: Colors.blueGrey),
        primarySwatch: Colors.blue,
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
            //primary: Colors.red,
            onPrimary: Colors.white,
            padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 30),
            textStyle: const TextStyle(
              //fontStyle: FontStyle.normal,
              fontWeight: FontWeight.bold,
              fontSize: 18,
              color: Colors.white,
            ),
          ),
        ),
        appBarTheme: const AppBarTheme(
          //foregroundColor: Colors.white,
          elevation: 5,
          titleTextStyle: TextStyle(
            fontSize: 24,
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}
