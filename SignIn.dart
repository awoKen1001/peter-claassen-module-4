import 'dart:html';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
//import 'package:laundryapp/Clients.dart';
import 'package:laundryapp/HomeScreen.dart';
import 'package:laundryapp/Register.dart';
import 'package:laundryapp/main.dart';

class SignIn extends StatefulWidget {
  Clients currentList;
  SignIn({Key? key, required this.currentList});

  @override
  _SignInState createState() => _SignInState(currentList);
}

class _SignInState extends State<SignIn> {
  var signInValue;
//class SignIn extends StatelessWidget {
  _SignInState(this.signInValue);
  //SignIn({Key? key}) : super(key: key);
  bool found = false;
  final tecUser = TextEditingController();
  final tecPassword = TextEditingController();
  String? feedback = "";
  String? message = "";

  final userName = "";

  Clients user = Clients();

  @override
  Widget build(BuildContext context) {
    _getUserList(signInValue);
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: const Text("Sign-In"),
        centerTitle: true,
      ),
      body: Column(
        children: [
          SizedBox(
            height: 115,
            width: 115,
            child: Stack(),
          ),
          Container(
            padding: const EdgeInsets.all(10),
            child: Text("$feedback"),
          ),
          Container(
            padding: const EdgeInsets.all(10),
            //color: Colors.lightBlue,
            child: Column(
              children: [
                const Text("CURRENT USERS AND PASSWORDS"),
                //Text("${user.nameList} ${user.passwordList}"),
                _displaySavedUsers(),
              ],
            ),
            // child: Text("${user.nameList} ${user.passwordList}"),
          ),
          Container(
            padding: const EdgeInsets.all(10),
            child: TextField(
              //const TextField(
              controller: tecUser,
              obscureText: false,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Username',
              ),
              onTap: (_updateFeedback),
            ),
          ),
          Container(
            padding: const EdgeInsets.all(10),
            child: TextField(
              controller: tecPassword,
              obscureText: true,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Password',
              ),
              onTap: (_updateFeedback),
            ),
          ),
          Center(
            child: Container(
              width: 225,
              padding: const EdgeInsets.all(10),
              child: ElevatedButton(
                onPressed: () {
                  for (int i = 0; i < user.userList.length; i++) {
                    if (tecUser.text == user.userList[i]) {
                      //found = true;
                      user.currentIndex = i;
                    } // else {
                    //   message = "No Match!!!";
                    //   _onLoginPressed(message);
                    //   _loginValuesPassed(context);
                    // }

                    //if (found = true) {
                    for (int i = 0; i < user.passwordList.length; i++) {
                      if (user.passwordList[user.currentIndex] ==
                              tecPassword.text &&
                          user.userList[user.currentIndex] == tecUser.text) {
                        message = "Match Found";
                        found = true;
                        //user.currentIndex = i;

                        // Clients userToPass = new Clients.createnew(true);
                        // userToPass.nameList
                        //     .add(user.nameList[user.currentIndex]);
                        // userToPass.userList
                        //     .add(user.userList[user.currentIndex]);
                        // userToPass.passwordList
                        //     .add(user.passwordList[user.currentIndex]);
                        _onLoginPressed(message);
                        _loginValuesPassed(context);
                        //Clients userToPass;

                        Navigator.push(
                            context,
                            //   MaterialPageRoute(builder: (context) {
                            // return HomeScreen(); //(title: 'SecondPage');
                            MaterialPageRoute(
                                builder: (context) => HomeScreen(
                                      currentUser: user,
                                    )));
                        //break;
                      } else {
                        if (found == false) {
                          message = "Match not found";
                        }
                      }
                    }
                    //}

                  }
                  if (found == true) {
                    message = "Match found!!!";
                  } else {
                    message = "Match not found!!!";
                  }
                  _resetTextControls();
                  _onLoginPressed(message);
                  // for (int i = 0; i < user.userList.length; i++) {
                  //   if (tecUser.text == user.userList[0] &&
                  //       tecPassword.text == user.passwordList[0]) {
                  //     message = "";
                  //     _onLoginPressed(message);

                  //     _loginValuesPassed(context);

                  //     Navigator.push(
                  //         context,
                  //         //   MaterialPageRoute(builder: (context) {
                  //         // return HomeScreen(); //(title: 'SecondPage');
                  //         MaterialPageRoute(
                  //             builder: (context) => HomeScreen(
                  //                   currentUser: user,
                  //                 )));
                  //   } else {
                  //     message = "Incorrect Credentials";
                  //   }
                  // }

                  //_onLoginPressed(message);

                  // Navigator.push(context, MaterialPageRoute(builder: (context) {
                  //   return MainScreen(); //(title: 'SecondPage');
                  // }));
                },
                child: const Text("Login"),
              ),
            ),
          ),
          Center(
            child: Container(
              width: 225,
              padding: const EdgeInsets.all(10),
              child: ElevatedButton(
                onPressed: () {
                  // Navigator.push(context, MaterialPageRoute(builder: (context) {
                  //   return MainScreen(); //(title: 'SecondPage');
                  // }));
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) {
                    return Register(
                      baseList: user,
                    ); //(title: 'SecondPage');
                  }));
                },
                child: const Text("Register"),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Text _displaySavedUsers() {
    Text t = Text("");

    // for (int i = 0; i < user.nameList.length; i++) {
    //   t = Text("${user.nameList[i]} ${user.passwordList[i]}");
    // }
    t = Text("${user.userList} ${user.passwordList}");
    return t;
  }

  void _RefreshUserRecord() {
    setState(() {
      for (int i = 0; i < user.passwordList.length; i++) {
        if (user.passwordList[user.currentIndex] == tecPassword.text &&
            user.userList[user.currentIndex] == tecUser.text) {
          message = "Match Found";
          found = true;
          Clients userToPass = new Clients.createnew(true);
          userToPass.nameList.add(user.nameList[user.currentIndex]);
          userToPass.userList.add(user.userList[user.currentIndex]);
          userToPass.passwordList.add(user.passwordList[user.currentIndex]);
          _onLoginPressed(message);
          _loginValuesPassed(context);
        }
      }
      // userToPass.nameList.add(user.nameList[user.currentIndex]);
      //     userToPass.userList.add(user.userList[user.currentIndex]);
      //     userToPass.passwordList.add(user.passwordList[user.currentIndex]);
    });
  }

  void _resetTextControls() {
    setState(() {
      tecUser.text = "";
      tecPassword.text = "";
      found = false;
    });
  }

  void _getUserList(Clients list) {
    // if (user.userList.length > 1) {
    //   user.nameList.add(list.nameList[0]);
    //   user.passwordList.add(list.passwordList[0]);
    //   user.userList.add(list.userList[0]);
    // }

    user = list;
  }

  void _onLoginPressed(String? _message) {
    setState(() {
      feedback = _message;
      //user.currentIndex = 0;
      //found = false;
    });
  }

  void _loginValuesPassed(BuildContext context) async {}

  void _updateFeedback() {
    setState(() {
      feedback = "";
    });
  }
}

class Clients {
  //var userList;
  //var passwordList;
  int currentIndex = 0;
  List<String?> userList = [];
  List<String?> passwordList = [];
  List<String?> nameList = [];
  int p = 0;

  Clients() {
    userList.add("default");
    passwordList.add("12345");
    nameList.add("default1");
  }

  Clients.createnew(bool dummy) {}
}
